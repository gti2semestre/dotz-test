# Desafio Dotz

Esse projeto ilustra a estrutura de dados gerada a partir do GCS (Google Cloud Storage), utilizando o Dataflow para realizar a leitura e carga, e o BigQuery para consumir os dados.

## Decisões de arquitetura: 
* Partindo de que os dados estão no GCS (Google Cloud Storage) e que já existe um Dataset no BigQuery chamado "tube_data", criei um pipeline via Dataflow e Python por um modelo de programação unificada (Apache BEAM) a fim de ler os dados no GCS e fazer a carga no BigQuery. 
Foi escolhido o Apache BEAM pelo fato de conseguir executar o mesmo código de criação de pipeline em outras ferramentas, por exemplo: Spark, Samza, Flink, entre outros.  

## Instruções de execução:
*  Acessar o Cloud Shell via GCP (Google Cloud Plataform) e executar os comandos em ordem sequencial contidos no arquivo criacao_ambiente_cloudshell.sh;
* Abrir o Code Editor no Cloud Shell e copiar o código do arquivo load_pricequote.py e colar no arquivo load-pricequote.py criado no primeiro passo. 
* Copiar o código do arquivo load_billofmaterials.py e colar no arquivo load-billofmaterials.py criado no primeiro passo. 
* Copiar o código do arquivo load_compboss.py e colar no arquivo load-compboss.py criado no primeiro passo. 
* Via IAM & Admin do GCP, acessar "Service Accounts" e realizar o donwload da chave de autenticação formato JSON. Feito isso, incluir esse arquivo JSON no diretório do Cloud Shell e apontar em cada código PY (Python) na linha 9 "os.environ" o PATH desse arquivo. 
* Executar os seguintes comandos via Cloud Shell para a criação do pipeline de cada tabela:
 1º Comando:
 python load_pricequote.py \
--project=$PROJECT \
--runner=DataflowRunner \
--staging_location=gs://$PROJECT/pricequote \
--temp_location gs://$PROJECT/pricequote_temp \
--input 'gs://dotz-project-2/pricequote/*.csv' \
--save_main_session

2º Comando:
python load_billofmaterials.py \
--project=$PROJECT \
--runner=DataflowRunner \
--staging_location=gs://$PROJECT/billofmaterials \
--temp_location gs://$PROJECT/billofmaterials_temp \
--input 'gs://dotz-project-2/billofmaterials/*.csv' \
--save_main_session

3º Comando:
python load_compboss.py \
--project=$PROJECT \
--runner=DataflowRunner \
--staging_location=gs://$PROJECT/compboss \
--temp_location gs://$PROJECT/compboss_temp \
--input 'gs://dotz-project-2/compboss/*.csv' \
--save_main_session
