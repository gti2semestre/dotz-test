# Introduzindo valor para a variável de PROJETO
export PROJECT=dotz-project

# Criando um diretório para gravar o programa de pipeline.
mkdir dotz-project

# Criando o arquivo Python de Price Quote.
touch load-pricequote.py

# Criando o arquivo Python de Bill of Materials.
touch load-billofmaterials.py

# Criando o arquivo Python de Comp Boss.
touch load-compboss.py

# Instalando a biblioteca apache_beam (caso já não tenha instalado)
python -m pip install apache_beam